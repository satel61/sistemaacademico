package view;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;

import org.junit.jupiter.api.Test;

import daof.DaoLogin;
import modelof.Alunof;

class TestandoView {

	@Test
	public void testLogin() throws SQLException {
		DaoLogin dl = new DaoLogin();
		assertEquals("Caso1", true, dl.validaSenha(1000, "123"));
	}

	@Test
	public void testAluno() throws SQLException {
		DaoLogin dl = new DaoLogin();
		assertNotNull(dl.dadosAluno(1000));
	}

	@Test
	public void testTurma() throws SQLException {
		DaoLogin dl = new DaoLogin();
		assertNotNull(dl.dadosTurma(1000));
	}

}
